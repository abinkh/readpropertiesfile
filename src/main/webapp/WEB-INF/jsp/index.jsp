<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index Page</title>
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
</head>
<body>
<h2>testing files</h2>
<div id="root"></div>
<script type="text/babel">

    ReactDOM.render(
            <div>
                <h1>React Render is here...</h1>
                <h2>${test}</h2>
                <h6>${test2}</h6>
            </div>,
        document.getElementById('root')
    );

</script>
</body>
</html>