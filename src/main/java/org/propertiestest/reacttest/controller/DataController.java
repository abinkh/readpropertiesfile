package org.propertiestest.reacttest.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
public class DataController {
    @Value("${message.first}")
    String test;
    @Value("${my.test}")
    String test2;

    @GetMapping("/getProperties")
    public List<Map<Object,Object>> test(Model model){
        model.addAttribute("test",test);
        model.addAttribute("test2",test2);
        Map map1=new LinkedHashMap();
        Map map2=new LinkedHashMap();
        map1.put("test",test);
        map2.put("my.test",test2);
        List<Map<Object,Object>> list=new LinkedList<>();
        list.add(map1);
        list.add(map2);
        return list;
    }
}
